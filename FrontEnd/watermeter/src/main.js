import BootstrapVue from 'bootstrap-vue'
import round from 'vue-round-filter'

import Vue from 'vue'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);

Vue.filter('round', round);

new Vue({
  el: '#app',
  render: h => h(App),
});
