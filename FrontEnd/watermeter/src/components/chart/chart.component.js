import VueCharts from 'vue-chartjs'
import { Bar, Line } from 'vue-chartjs'

export default {
  extends: Bar,
  name: 'chart',
  components: {}, 
  props: ["data", "options"],
  data () {
    return {

    }
  },
  computed: {

  },
  mounted() {
    this.renderChart({
   //   maintainAspectRatio: false,
      labels: [
        'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',
        'December'
      ],
      datasets: [
        {
          label: 'hot',
          backgroundColor: '#f83030',
          data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 200, 12, 11]
        },
        {
        label: 'cold',
        backgroundColor: '#000079',
        data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]
      }
      ]
    });

  },
  methods: {

  }
}
