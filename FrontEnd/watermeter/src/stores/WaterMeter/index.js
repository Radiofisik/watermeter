import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import config from "../../config"

Vue.use(Vuex);

let store = new Vuex.Store({
  state: {
    hotWater: 0,
    coldWater: 0
  },

  mutations: {
    setHotWater(state, payload) {
      state.hotWater = payload;
    },
    setColdWater(state, payload) {
      state.coldWater = payload;
    }
  },

  getters: {
    getHotWater(state) {
      return config.ORIGINALHOT + state.hotWater * config.MULTIPLIERHOT;
    },
    getColdWater(state) {
      return config.ORIGINALCOLD + state.coldWater * config.MULTIPLIERCOLD;
    }
  },

  actions: {
    init({ dispatch, state }) {
      dispatch('getSensorData');
    },
    getSensorData({ commit, state }) {

      ////TODO: fix firmware to delete it
      //axios.interceptors.response.use(function (response) {
      //  var respData = response.data || "";
      //  if (typeof (respData) === "string") {
      //    console.log(respData);
      //    respData = respData.replace("hot", '"hot"');
      //    respData = respData.replace("cold", '"cold"');
      //    response.data = JSON.parse(respData);
      //  }
      //  return response;
      //});

      axios.get(config.SENSORURL, { params: {} })
        .then(response => {
          commit('setHotWater', response.data.hot);
          commit('setColdWater', response.data.cold);
        })
        .catch(e => {
          console.log(e);
        });
    }
  }

});

export default store;
