﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using Swashbuckle.AspNetCore.Swagger;
using WaterMeter.AutoMapperProfiles;
using WaterMeter.DAL;
using WaterMeter.Interfaces;
using WaterMeter.Repository;
using WaterMeter.Services;
using WaterMeter.Services.Mock;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace WaterMeter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var myConfiguration = new WaterMeterConfig();
            Configuration.GetSection("WaterMeterConfig").Bind(myConfiguration);
            services.AddSingleton<WaterMeterConfig>(myConfiguration);

            services.AddAutoMapper();

            var connectionString = Configuration.GetConnectionString("Database");
            services.AddDbContext<WaterMeterContext>(x => x.UseSqlite(connectionString), ServiceLifetime.Transient);

            services.AddTransient<IWaterMeterRepository, WaterMeterRepository>();
            services.AddTransient<IWaterMeterRestService, WaterMeterRestService>();
//            services.AddTransient<IWaterMeterRestService, WaterMeterMockRestService>();
            services.AddSingleton<IHostedService, WaterMeterUpdateService>();
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "WaterMeter HTTP API",
                    Version = "v1",
                    Description = "The WaterMeter Service HTTP API",
                    TermsOfService = "Terms Of Service"
                });
            });

            var settings = new ConnectionSettings(new Uri("http://elastic:9200"))
                .DefaultIndex("watermeter");
            var client = new ElasticClient(settings);
            services.AddSingleton<IElasticClient>(client);
            services.AddTransient<IWaterMeterElasticRepository, ElasticRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();

            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/v1/swagger.json", "WaterMeter.API V1");
                    c.OAuthClientId("wateermeterswaggerui");
                    c.OAuthAppName("Watermeter Swagger UI");
                });
        }

      
    }
}
