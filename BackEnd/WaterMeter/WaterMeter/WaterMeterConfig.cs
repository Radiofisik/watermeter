﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterMeter
{
    public class WaterMeterConfig
    {
        public double OriginalHot { get; set; }
        public double OriginalCold { get; set; }
        public string Url { get; set; }
        public int Period { get; set; }
    }
}
