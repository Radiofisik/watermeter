﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WaterMeter.DTO;
using WaterMeter.Interfaces;
using WaterMeter.Repository;

namespace WaterMeter.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IWaterMeterRepository _waterMeterRepository;
        private readonly IMapper _mapper;

        public ValuesController(IWaterMeterRepository waterMeterRepository, IMapper mapper)
        {
            this._waterMeterRepository = waterMeterRepository;
            _mapper = mapper;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<WaterMeterOutDto> Get()
        {
            var entities = _waterMeterRepository.GetAll();
            var dtos = _mapper.Map<IList<WaterMeterOutDto>>(entities);
            return dtos;
        }

        /*
        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */
    }
}