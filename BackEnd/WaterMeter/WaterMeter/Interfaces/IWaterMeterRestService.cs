﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterMeter.DTO;

namespace WaterMeter.Interfaces
{
    public interface IWaterMeterRestService
    {
        Task<WaterMeterDto> GetData();
    }
}
