﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterMeter.Entyty;

namespace WaterMeter.Interfaces
{
    public interface IWaterMeterRepository: IRepository<WaterMeterEntity>
    {
    }
}
