﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterMeter.DTO;
using WaterMeter.Interfaces;

namespace WaterMeter.Services.Mock
{
    public class WaterMeterMockRestService: IWaterMeterRestService
    {
        public async Task<WaterMeterDto> GetData()
        {
           return new WaterMeterDto(){Cold = 555555555, Hot = 88888888888};
        }
    }
}
