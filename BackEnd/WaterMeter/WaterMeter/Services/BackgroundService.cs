﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WaterMeter.Services
{
    public abstract class BackgroundService : IHostedService, IDisposable
    {
        private Task executingTask;

        private readonly CancellationTokenSource stoppingCts = new CancellationTokenSource();

        protected abstract Task ExecuteAsync(CancellationToken stoppingToken);

        private ILogger logger;

        protected BackgroundService(ILogger logger)
        {
            this.logger = logger;
        }

        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("startAsync called");
            executingTask = ExecuteAsync(stoppingCts.Token);
            if (executingTask.IsCompleted)
            {
                return executingTask;
            }
            return Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("stopAsync called");
            if (executingTask == null)
            {
                return;
            }
            try
            {
                stoppingCts.Cancel();
            }
            finally
            {
                await Task.WhenAny(executingTask, Task.Delay(Timeout.Infinite,
                    cancellationToken));
            }
        }

        public virtual void Dispose()
        {
            stoppingCts.Cancel();
            logger.LogInformation("background service disposed");
        }
    }
}
