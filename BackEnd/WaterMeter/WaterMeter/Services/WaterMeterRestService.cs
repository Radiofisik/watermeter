﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WaterMeter.DTO;
using WaterMeter.Interfaces;

namespace WaterMeter.Services
{
    public class WaterMeterRestService: IWaterMeterRestService
    {
        private readonly WaterMeterConfig config;

        public WaterMeterRestService(WaterMeterConfig config)
        {
            this.config = config;
        }

        public async Task<WaterMeterDto> GetData()
        {
            var httpClient = new HttpClient();

            var waterMeterJson = await httpClient.GetStringAsync(config.Url);

            var waterMeterDto = JsonConvert.DeserializeObject<WaterMeterDto>(waterMeterJson);

            System.Diagnostics.Debug.WriteLine($"hot={waterMeterDto.Hot} cold={waterMeterDto.Cold}");

            return waterMeterDto;
        }
    }
}
