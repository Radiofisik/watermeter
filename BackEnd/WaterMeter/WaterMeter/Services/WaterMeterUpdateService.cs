﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WaterMeter.DTO;
using WaterMeter.Entyty;
using WaterMeter.Interfaces;

namespace WaterMeter.Services
{
    public class WaterMeterUpdateService : BackgroundService
    {
        private readonly ILogger<WaterMeterUpdateService> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly WaterMeterConfig _config;

        public WaterMeterUpdateService(IServiceScopeFactory scopeFactory, ILogger<WaterMeterUpdateService> logger,
            WaterMeterConfig config, IWaterMeterElasticRepository waterMeterElasticRepository) : base(logger)
        {
            this._scopeFactory = scopeFactory;
            this._logger = logger;
            this._config = config;
        }

        private async Task UpdateDB()
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    IWaterMeterRepository waterMeterRepository =
                        scope.ServiceProvider.GetRequiredService<IWaterMeterRepository>();
                    IWaterMeterElasticRepository waterMeterElasticRepository =
                        scope.ServiceProvider.GetRequiredService<IWaterMeterElasticRepository>();
                    IWaterMeterRestService waterMeterRestService =
                        scope.ServiceProvider.GetRequiredService<IWaterMeterRestService>();
                    IMapper mapper = scope.ServiceProvider.GetRequiredService<IMapper>();

                    var dto = await waterMeterRestService.GetData();
                    var entity = mapper.Map<WaterMeterDto, WaterMeterEntity>(dto);
                    entity.OriginCold = _config.OriginalCold;
                    entity.OriginHot = _config.OriginalHot;
                    waterMeterRepository.Add(entity);
                    waterMeterElasticRepository.Add(entity);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "unforseen error in UpdateDB");
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() => { _logger.LogInformation("Update service has started"); });

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("try to update DB");
                await UpdateDB();
                await Task.Delay(1000 * _config.Period, stoppingToken);
            }
        }
    }
}