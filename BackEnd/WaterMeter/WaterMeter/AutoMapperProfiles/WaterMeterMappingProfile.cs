﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WaterMeter.DTO;
using WaterMeter.Entyty;
using WaterMeter.Repository;

namespace WaterMeter.AutoMapperProfiles
{
    public class WaterMeterMappingProfile: Profile
    {
        public WaterMeterMappingProfile()//(WaterMeterConfig config)
        {
            CreateMap<WaterMeterEntity, WaterMeterDto>();
            CreateMap<WaterMeterDto, WaterMeterEntity>().AfterMap((x, y) =>
            {
                y.Time = DateTime.Now;
//                y.OriginCold = config.OriginalCold;
//                y.OriginHot = config.OriginalHot;
            });
            CreateMap<WaterMeterEntity, WaterMeterOutDto>();
        }
    }
}
