﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WaterMeter.DAL;
using WaterMeter.Entyty;
using WaterMeter.Interfaces;

namespace WaterMeter.Repository
{
    public class WaterMeterRepository: BaseRepository<WaterMeterEntity>, IWaterMeterRepository
    {
        public WaterMeterRepository(WaterMeterContext context) : base(context)
        {
        }
    }
}
