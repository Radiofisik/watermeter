﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using WaterMeter.Entyty;
using WaterMeter.Interfaces;

namespace WaterMeter.Repository
{
    public class ElasticRepository: IWaterMeterElasticRepository
    {
        private readonly IElasticClient _client;

        public ElasticRepository(IElasticClient client)
        {
            _client = client;
        }

        public IList<WaterMeterEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Add(WaterMeterEntity entity)
        {
            _client.IndexDocument(entity);
        }

        public void Update(WaterMeterEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(WaterMeterEntity entity)
        {
            throw new NotImplementedException();
        }

        public WaterMeterEntity Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}
