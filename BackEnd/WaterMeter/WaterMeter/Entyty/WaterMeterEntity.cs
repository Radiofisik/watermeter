﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaterMeter.Entyty
{
    public class WaterMeterEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime Time { get; set; }

        public long Cold { get; set; }

        public long Hot { get; set; }

        public double OriginHot { get; set; }

        public double OriginCold { get; set; }
    }
}
