﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterMeter.DTO
{
    public class WaterMeterDto
    {
        public long Cold { get; set; }
        public long Hot { get; set; }
    }
}
