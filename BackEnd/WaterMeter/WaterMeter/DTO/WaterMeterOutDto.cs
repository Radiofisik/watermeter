﻿using System;

namespace WaterMeter.DTO
{
    public class WaterMeterOutDto
    {
        public DateTime Time { get; set; }

        public long Cold { get; set; }

        public long Hot { get; set; }

        public double OriginHot { get; set; }

        public double OriginCold { get; set; }
    }
}
