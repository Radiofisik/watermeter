﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WaterMeter.Entyty;

namespace WaterMeter.DAL
{
    public class WaterMeterContext: DbContext
    {
        public WaterMeterContext(DbContextOptions<WaterMeterContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<WaterMeterEntity> Entries { get; set; }
    }
}
