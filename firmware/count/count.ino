#define GPIO_HOT 14 
#define GPIO_COLD 12
#define LED 2
#define HOT_STORE_ADDR 0
#define COLD_STORE_ADDR 4

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <EEPROM.h> 

const char *ssid = "RFNet";
const char *password = "iojusiuh8iey8w654$#^%*&^*&(*&!@@!gftyws7urg7egru";

//This function will write a 4 byte (32bit) long to the eeprom at
//the specified address to address + 3.
void EEPROMWritelong(int address, long value)
      {
      //Decomposition from a long to 4 bytes by using bitshift.
      //One = Most significant -> Four = Least significant byte
      byte four = (value & 0xFF);
      byte three = ((value >> 8) & 0xFF);
      byte two = ((value >> 16) & 0xFF);
      byte one = ((value >> 24) & 0xFF);

      //Write the 4 bytes into the eeprom memory.
      EEPROM.write(address, four);
      EEPROM.write(address + 1, three);
      EEPROM.write(address + 2, two);
      EEPROM.write(address + 3, one);
      }

      
long EEPROMReadlong(long address)
      {
      //Read the 4 bytes from the eeprom memory.
      long four = EEPROM.read(address);
      long three = EEPROM.read(address + 1);
      long two = EEPROM.read(address + 2);
      long one = EEPROM.read(address + 3);
      
      //Return the recomposed long by using bitshift.
      return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
      }


ESP8266WebServer server(80);

uint8_t led = LOW;

volatile byte interuptCountHot;
long commonInteruptCountHot;

volatile byte interuptCountCold;
long commonInteruptCountCold;

void handleRoot() {
  server.send(200, "text/json", "{\"hot\":"+ String(commonInteruptCountHot)+", \"cold\":"+ String(commonInteruptCountCold)+"}");
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(){
    pinMode(GPIO_HOT, INPUT);
    pinMode(GPIO_COLD, INPUT);
    pinMode(LED, OUTPUT);
    setLED();

    EEPROM.begin(512);
    
    interuptCountHot = 0;
    commonInteruptCountHot = EEPROMReadlong(HOT_STORE_ADDR);
    interuptCountCold = 0;
    commonInteruptCountCold = EEPROMReadlong(COLD_STORE_ADDR);
    
    attachInterrupt(GPIO_HOT, hotInterupt, RISING);
    attachInterrupt(GPIO_COLD, coldInterupt, RISING);

    Serial.begin(115200);
    WiFi.begin(ssid, password);
    Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/reset", [](){
    server.send(200, "text/plain", "reset eeprom");
    EEPROMWritelong(COLD_STORE_ADDR, 0);
    EEPROMWritelong(HOT_STORE_ADDR, 0);
    commonInteruptCountHot = 0;
    commonInteruptCountCold = 0;
    EEPROM.commit();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void hotInterupt(){
    led = !led;
    interuptCountHot++;
}

void coldInterupt(){
    led = !led;
    interuptCountCold++;
}

void setLED(){
    digitalWrite(LED, led);
}

void actualizeInteruptCount(){
  commonInteruptCountHot += interuptCountHot;
  commonInteruptCountCold += interuptCountCold;
  interuptCountHot = 0;
  interuptCountCold = 0;
  if(commonInteruptCountHot - EEPROMReadlong(HOT_STORE_ADDR) > 65*10){
    EEPROMWritelong(HOT_STORE_ADDR, commonInteruptCountHot);
    EEPROM.commit();
  }

   if(commonInteruptCountCold - EEPROMReadlong(COLD_STORE_ADDR) > 65*10){
    EEPROMWritelong(COLD_STORE_ADDR, commonInteruptCountCold);
    EEPROM.commit();
  }
}

void loop(){
    setLED();  
    actualizeInteruptCount();  
    server.handleClient();
}
